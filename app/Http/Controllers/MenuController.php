<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Http\Requests\StoreMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\DataTables\MenusDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MenusDataTable $dataTable)
    {
        return $dataTable->render('backend.menu.menu-list');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('backend.menu.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'required|image',
            'price' => 'required|numeric',
        ]);


        //Upload Image
        $imageName = time().'-'.$request->name.'.'.$request->image->extension();  
        // $request->image->move(public_path('images'), $imageName);
        $request->image->storeAs('public/image/' . $imageName);

        //Create Menu
        Menu::create([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $imageName,
            'price' => $request->price,
            'status' => 'unavailable'
        ]);

        return redirect(route('menu.list'))->with('success', 'Your data has been saved');
    }

    /**
     * Display the specified resource.
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Menu $menu, $id)
    {
        $menu = Menu::find($id);
        return view('backend.menu.edit', [
            'menu' => $menu
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMenuRequest $request, Menu $menu, $id)
    {
        if (Menu::find($id)) {
            $validatedData = $request->validate([
                'name' => 'required',
                'description' => 'required',
                'image' => 'required|image',
                'price' => 'required|numeric',
            ]);
        }
        $dataUpdate = $menu->find($id);
        
        if ($dataUpdate->image ==! null) {
            Storage::delete(asset('/image'.$dataUpdate->image));
            $imageName = time().'.'.$request->image->extension();  
            $request->image->storeAs('public/image/' . $imageName);
        } else {
            $imageName = time().'.'.$request->image->extension();  
            $request->image->storeAs('public/image/' . $imageName);
        }
        
        // Upload Image
        

        //Update Menu
        $dataUpdate->update([
            'name' => $request->name,
            'description' => $request->description,
            'image' => $imageName,
            'price' => $request->price,
            'status' => $dataUpdate->status,
        ]);

        return redirect(route('menu.list'))->with('success', 'Your data has been saved');
    }

    public function statusUpdate($id, Request $request)
    {
        $menu = Menu::find($id);
        $menu->status = $request->input('status');
        $menu->save();

        return redirect()->route('menu.list')->with('success', 'Your ' . $menu->name . ' status has been change to ' . $menu->status);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Menu $menu, $id)
    {
        $menu = Menu::find($id);

        if ($menu->image ==! null) {
            $menu->delete();
        } else {
            Storage::delete(asset('/image'.$dataUpdate->image));
            $menu->delete();
        }  

        return redirect()->back()->with('success', 'Your data has been deleted');
    }
}
