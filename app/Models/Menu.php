<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'description',
        'image',
        'price',];

    public function Menu(): HasMany 
    {
        return $this->hasMany(Menu::class, 'menu_id', 'id');
    }
}
