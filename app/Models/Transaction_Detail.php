<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction_Detail extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function Transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    public function Menu(): BelongsTo
    {
        return $this->belongsTo(Menu::class);
    }
}
