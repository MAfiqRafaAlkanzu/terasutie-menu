<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Desk extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function Desk(): HasMany
    {
        return $this->hasMany(Desk::class, 'desk_id', 'id');
    }
}
