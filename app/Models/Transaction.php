<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function Transaction(): HasMany
    {
        return $this->hasMany(Transaction::class, 'transaction_id', 'id');
    }

    public function Desk(): BelongsTo
    {
        return $this->belongsTo(Desk::class);
    }
}
