<?php

namespace App\DataTables;

use App\Models\Menu;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MenusDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->setRowId('id')
            ->addColumn('action', function ($data) {
                return '<div class="dt-buttons btn-group flex-wrap"><button class="btn btn-danger type="button" data-id="'.$data['id'].'" id="deleteButton" type="submit">Delete</button><a href="'.route("menu.edit", ["id" => $data['id']]).'" class="btn btn-info text-light" id="editButton" type="button" data-id="'.$data['id'].'"><span>Edit</span></a></div>';
            })
            ->editColumn('image', function ($data) {
                return '<img src="'.asset('storage/image/'.$data['image']).'" max-width="100" height="50" class="rounded mx-auto d-block" alt="'.$data->name.'">';
            })
            ->editColumn('status', function ($data) {
                $selectedAvailable = isset($data['status']) && $data['status'] === 'available' ? 'selected' : '';
                $selectedUnavailable = isset($data['status']) && $data['status'] === 'unavailable' ? 'selected' : '';   

                $bgClass = $data['status'] === 'available' ? 'bg-success' : 'bg-danger';

                return '<select name="status" id="status" class="form-control mx-auto my-auto '.$bgClass.'" data-id="'.$data['id'].'"><option value="available" '.$selectedAvailable.' class="bg-success">Available</option><option value="unavailable" '.$selectedUnavailable.' class="bg-danger">Unavailable</option></select>';
            })
            ->rawColumns(['image', 'action', 'status']);
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Menu $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->orderBy(0, 'asc')
                    ->buttons([
                        Button::make('add'),
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('id')
                ->visible(false),
            Column::make('name'),
            Column::make('description')
                    // ->addClass('text-truncate')
                    ->style('max-width:350px'),
            Column::make('image')
                    ->orderable(false),
            Column::make('price'),
            Column::make('status')
                    ->addClass('my-auto'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center')
                  ->title('Action')
                  ->searchable(false)
                  ->oderable(false)
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Menus_' . date('YmdHis');
    }
}
