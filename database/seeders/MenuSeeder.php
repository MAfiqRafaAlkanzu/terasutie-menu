<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker\Factory::create();

        Menu::insert([
            'name' => Str::random(10),
            'description' => Str::random(10),
            'image' => Str::random(10),
            'price' => $faker->randomDigit(),
            'status' => 'available'
        ]);
    }
}
