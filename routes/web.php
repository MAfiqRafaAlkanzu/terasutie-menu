<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/dashboard')->middleware(['auth', 'verified'])->group(function (){
    Route::get('/', function(){ return view('dashboard');})->name('dashboard');

    Route::prefix('menu')->group(function(){
        Route::get('/', [MenuController::class,'index'])->name('menu.list');
        Route::get('/data', [MenuController::class,'data'])->name('menu.data-table');
        Route::get('/create', [MenuController::class,'create'])->name('menu.create');
        Route::get('/edit/{id}', [MenuController::class,'edit'])->name('menu.edit');
        Route::post('/store', [MenuController::class,'store'])->name('menu.store');
        Route::post('/delete/{id}', [MenuController::class, 'destroy'])->name('menu.delete');
        Route::post('/update/{id}', [MenuController::class, 'update'])->name('menu.update');
        Route::post('/status-update/{id}', [MenuController::class, 'statusUpdate'])->name('menu.statusUpdate');
    });
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
