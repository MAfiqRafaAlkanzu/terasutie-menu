<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-dark dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <h2 class="text-center">Add New Menu</h2>
                    <form class="mt-5" action="{{ route('menu.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Menu Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Menu Name" name="name">
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                          </div>
                          <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" id="description" rows="3" name="description"></textarea>
                            @error('description')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                          </div>
                          <div class="mb-3 row">
                            <div class="col">
                                <label for="image" class="form-label">Image</label>
                                <input class="form-control @error('image') is-invalid @enderror" type="file" id="image" name="image" accept="image/jpg,image/png, image/jpeg" onchange="showPreview(event)">
                                @error('image')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col d-flex justify-content-center">
                                <img src="" alt="Menu Image" id="imgPreview" class="hidden border border-primary-subtle" style="max-width: 250px;max-height:250px">
                            </div>
                          </div>
                          <div class="mb-3">
                            <label for="price" class="form-label @error('price') is-invalid @enderror">Menu Price</label>
                            <input type="text" class="form-control" id="price" placeholder="Menu Price" name="price">
                            @error('price')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                          </div>
                          <div class="d-flex mb-3 align-items-center">
                            <div class="mb3 m-auto">
                                <button type="submit" class="btn btn-success m-auto">Submit</button>
                                <button type="button" class="btn btn-danger m-auto" onclick="history.back()">Cancel</button>
                            </div>
                          </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            function showPreview(event){
                if(event.target.files.length > 0){
                    var src = URL.createObjectURL(event.target.files[0]);
                    var preview = document.getElementById("imgPreview");
                    preview.src = src;
                    preview.style.display = "block";
                }
            }
        </script>
    @endpush
</x-app-layout>
