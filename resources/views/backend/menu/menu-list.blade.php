<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Menu') }}
        </h2>
        <div class="col-12">
            {!! display_bootstrap_alerts() !!}
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-dark dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="card-body">
                        {{ $dataTable->table(['class' => 'bg-dark', 'id' => 'menuDatatable']) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
        {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

        @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            $(document).ready(function() {
                $(document).on('change', '#status', function (event) {
                    var statusId = $(this).data('id');
                    var selectedValue = $(this).val();
                    event.preventDefault();
                    // Define the update URL with the dynamic item ID
                    var updateUrl = "{{ route('menu.statusUpdate', ['id' => ':statusId']) }}".replace(':statusId', statusId);

                    // Trigger AJAX request when the dropdown value changes
                    $.ajax({
                        url: updateUrl,
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: statusId,
                            status: selectedValue
                        },
                        success: function(data) {deleteButton
                            console.log('Success:', data);
                            // Optionally, refresh the DataTable after updating the status
                            $('#menuDatatable').DataTable().ajax.reload();
                        },
                        error: function(data) {
                            console.log('Error:', data);
                        }
                    });
                });

                $(document).on('click', "#deleteButton", function(event){
                        event.preventDefault();
                        // Get menu id 
                        var menuId = $(this).data('id');
                        // Define the delete URL with the dynamic item ID
                        var deleteUrl = "{{ route('menu.delete', ['id' => ':menuId']) }}".replace(':menuId', menuId);
                        $.ajax({
                            url: deleteUrl,
                            type: 'POST',
                            data: {
                                _token: "{{ csrf_token() }}",
                                id: menuId,
                            },
                            success: function (data) {
                                console.log('Success:', data);
                                $('#menuDatatable').DataTable().ajax.reload();
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            },
                        }); 
                    });                
            });
        </script>
        @endpush
</x-app-layout>
