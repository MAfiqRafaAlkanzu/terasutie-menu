function initDataTable() {
    $('#myTable').DataTable({
        serverSide: true,
        ajax: route('menu-list'),
        columns: [
            { data: 'id' },
            { data: 'name' },
            // Other Columns
            {
                data: null,
                render: function (data, type, row) {
                    return '<button class="btn btn-danger delete-btn" data-id="' + row.id + '">Delete</button>';
                },
            },
        ],
    });
}

function handleDeleteButtonClick() {
    $(document).on('click', '.delete-btn', function () {
        var userId = $(this).data('id');

        $.ajax({
            url: '/delete-user/' + userId,
            type: 'DELETE',
            success: function (data) {
                // Handle success, maybe refresh the DataTables or update the row
            },
            error: function (xhr, status, error) {
                // Handle errors
            },
        });
    });
}